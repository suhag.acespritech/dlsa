<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\BgImagesController;
use App\Http\Controllers\ToolsGadgetsController;
use App\Http\Controllers\BroucherWallpapersController;
use App\Http\Controllers\SocialLinksController;
use App\Http\Controllers\AuthController;
use App\http\Middleware\DlsMiddleware;

//login
Route::get('login',[AuthController::class,'login'])->name('login');
Route::post('postlogin',[AuthController::class,'postlogin'])->name('postlogin');
//Middleware
Route::middleware(['isAuth'])->group(function () {
//Dashboard
Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');
//logout
Route::get('logout',[AuthController::class,'logout'])->name('logout');
//Category
Route::get('index', [CategoriesController::class, 'index'])->name('category.index');
Route::get('/category/create', [CategoriesController::class, 'create'])->name('category.create');
Route::post('/category/store', [CategoriesController::class, 'store'])->name('category.store');
Route::get('/category/edit/{id}', [CategoriesController::class, 'edit'])->name('category.edit');
Route::put('/category/update/{id}', [CategoriesController::class, 'update'])->name('category.update');
Route::get('/category/delete/{id}', [CategoriesController::class, 'destroy'])->name('category.delete');
//Images
Route::get('/images/index', [ImagesController::class, 'index'])->name('image.index');
Route::get('/images/create', [ImagesController::class, 'create'])->name('image.create');
Route::post('/images/store', [ImagesController::class, 'store'])->name('image.store');
Route::get('/images/edit/{id}', [ImagesController::class, 'edit'])->name('image.edit');
Route::put('/images/update/{id}', [ImagesController::class, 'update'])->name('image.update');
Route::get('/images/delete/{id}', [ImagesController::class, 'destroy'])->name('image.delete');
//Background images
Route::get('/bg_images/index', [BgImagesController::class, 'index'])->name('bgimage.index');
Route::get('/bg_images/create', [BgImagesController::class, 'create'])->name('bgimage.create');
Route::post('/bg_images/store', [BgImagesController::class, 'store'])->name('bgimage.store');
Route::get('/bg_images/edit/{id}', [BgImagesController::class, 'edit'])->name('bgimage.edit');
Route::put('/bg_images/update/{id}', [BgImagesController::class, 'update'])->name('bgimage.update');
Route::get('/bg_images/delete/{id}', [BgImagesController::class, 'destroy'])->name('bgimage.delete');
//Tools Gadgets
Route::get('/tools_gadgets/index', [ToolsGadgetsController::class, 'index'])->name('toolsgadgets.index');
Route::get('/tools_gadgets/create', [ToolsGadgetsController::class, 'create'])->name('toolsgadgets.create');
Route::post('/tools_gadgets/store', [ToolsGadgetsController::class, 'store'])->name('toolsgadgets.store');
Route::get('/tools_gadgets/edit/{id}', [ToolsGadgetsController::class, 'edit'])->name('toolsgadgets.edit');
Route::put('/tools_gadgets/update/{id}', [ToolsGadgetsController::class, 'update'])->name('toolsgadgets.update');
Route::get('/tools_gadgets/delete/{id}', [ToolsGadgetsController::class, 'destroy'])->name('toolsgadgets.delete');
//Broucher Wallpapers
Route::get('/broucher_wallpapers/index', [BroucherWallpapersController::class, 'index'])->name('broucherwallpapers.index');
Route::get('/broucher_wallpapers/create', [BroucherWallpapersController::class, 'create'])->name('broucherwallpapers.create');
Route::post('/broucher_wallpapers/store', [BroucherWallpapersController::class, 'store'])->name('broucherwallpapers.store');
Route::get('/broucher_wallpapers/edit/{id}', [BroucherWallpapersController::class, 'edit'])->name('broucherwallpapers.edit');
Route::put('/broucher_wallpapers/update/{id}', [BroucherWallpapersController::class, 'update'])->name('broucherwallpapers.update');
Route::get('/broucher_wallpapers/delete/{id}', [BroucherWallpapersController::class, 'destroy'])->name('broucherwallpapers.delete');
//Social Links
Route::get('/social_links/index', [SocialLinksController::class, 'index'])->name('sociallinks.index');
Route::get('/social_links/create', [SocialLinksController::class, 'create'])->name('sociallinks.create');
Route::post('/social_links/store', [SocialLinksController::class, 'store'])->name('sociallinks.store');
Route::get('/social_links/edit/{id}', [SocialLinksController::class, 'edit'])->name('sociallinks.edit');
Route::put('/social_links/update/{id}', [SocialLinksController::class, 'update'])->name('sociallinks.update');
Route::get('/social_links/statusupdate/{id}', [SocialLinksController::class, 'statusupdate'])->name('sociallinks.statusupdate');
Route::get('/social_links/delete/{id}', [SocialLinksController::class, 'destroy'])->name('sociallinks.delete');

});
