<?php
use App\Http\Controllers\Api\DataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
 
Route::get('fetch-all-data', [DataController::class, 'fetchAllData']);
