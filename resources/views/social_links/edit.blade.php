@extends('Layouts.app')
@section('content')
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
     <!-- jQuery -->
     <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
     <!-- Bootstrap 4 -->
     <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
     <!-- AdminLTE App -->
     <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
     <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <style>
        .bootstrap-switch {
            border: 1px solid #ced4da;
            border-radius: .25rem;
            cursor: pointer;
            direction: ltr;
            display: inline-block;
            line-height: .5rem;
            overflow: hidden;
            position: relative;
            text-align: left;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            vertical-align: middle;
            z-index: 0;
        }
    </style>
    <script>
        $(document).ready(function() {
            $("input[data-bootstrap-switch]").each(function() {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            })
        });
    </script>

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">

                            <div class="col-sm-6">
                                <h1 class="m-0">Social Links </h1>
                                {{-- @if (\Session::has('success'))
                                    <div class="alert alert-primary">
                                        <ul>
                                            <li>{!! \Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                @endif
                                @if (\Session::has('fail'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{!! \Session::get('fail') !!}</li>
                                        </ul>
                                    </div>
                                @endif --}}
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Social Links</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <!-- small box -->
                                <div class="small-box bg-light">
                                    <form class="form-horizontal" method="POST"
                                        action="{{ route('sociallinks.update', $images->id) }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="label" class="col-sm-2 col-form-label">Link
                                                </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="link" class="form-control" id="link"
                                                        value="{{ $images->link }}" placeholder="Enter your link..">
                                                    @if ($errors->has('link'))
                                                        <span class="text-danger">{{ $errors->first('link') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="image" class="col-sm-2 col-form-label">Image
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="file" name="image" class="form-control" id="image"
                                                        placeholder="Select your image..">
                                                    @if ($errors->has('image'))
                                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-3">
                                                 <img src="/images/{{$images->image}}"height="150px" width="265px   " alt="" srcset="">
                                                </div>
                                            </div>
                                            <!-- Bootstrap Switch -->
                                            <div class="form-group row">
                                                <label for="status" class="col-sm-2 col-form-label">Status
                                                </label>
                                                <div class="card-body">
                                                    <input type="checkbox" name="status" <?php echo $images->status == 1 ? 'checked' : ''; ?>
                                                        data-bootstrap-switch>
                                                </div>
                                            </div>

                                        </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-outline-secondary">update</button>
                                    <a type="submit" href="{{ route('sociallinks.index') }}"
                                    class="btn btn-outline-danger float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                                </form>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
        </div>
          @if (\Session::has('fail'))
        <script>
            $(document).ready(function() {
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    icon: 'error',
                    title: '{!! \Session::get('fail') !!}'
                })
            });
        </script>
    @endif
    @if (\Session::has('success'))
        <script>
            $(document).ready(function() {
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    icon: 'success',
                    title: '{!! \Session::get('success') !!}'
                })
            });
        </script>
    @endif
    @endsection
