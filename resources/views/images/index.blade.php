@extends('Layouts.app')
@section('content')
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
     <!-- jQuery -->
     <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
     <!-- Bootstrap 4 -->
     <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
     <!-- AdminLTE App -->
     <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
     <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                {{-- <h1 class="m-0">Images</h1> --}}
                                {{-- @if (\Session::has('success'))
                                    <div class="alert alert-primary">
                                        <ul>
                                            <li>{!! \Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                @endif --}}

                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                                    <li class="breadcrumb-item active">Image</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <!-- small box -->
                                <div class="small-box  bg-light">
                                    <div class="card-header">
                                        <h3 class="card-title">Images</h3>
                                        <a type="submit" href="{{ route('dashboard') }}"
                                            class="btn btn-outline-danger  float-right ">Cancle</a>
                                        <a type="submit" href="{{ route('image.create') }}"
                                            class="btn btn-outline-primary float-right mr-1">Add More</a>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    {{-- <th style="width: 10px">ID</th> --}}
                                                    <th>Category Name</th>
                                                    <th>Label </th>
                                                    <th>Url</th>
                                                    <th>Link </th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($image as $images)
                                                    <tr>
                                                        {{-- <td>{{ $images->id }}</td> --}}
                                                        <td>{{ ucfirst($images->category->name) }}</td>
                                                        <td>{{ ucfirst($images->label) }}</td>
                                                        <td><a
                                                                href="{{ $images->url }}"class="single-line-link">{{ $images->url }}</a>
                                                        </td>
                                                        <td><a
                                                                href="{{ $images->link }}"class="single-line-link">{{ $images->link }}</a>
                                                        </td>
                                                        <td><a href="{{ route('image.edit', $images->id) }}"><span>
                                                                    <i class="fas fa-edit"></i>
                                                                </span></a>
                                                            <a href="{{ route('image.delete', $images->id) }}"><span>
                                                                <i class="fa fa-trash"></i></span></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        @if (\Session::has('fail'))
        <script>
            $(document).ready(function() {
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    icon: 'error',
                    title: '{!! \Session::get('fail') !!}'
                })
            });
        </script>
    @endif
    @if (\Session::has('success'))
        <script>
            $(document).ready(function() {
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    icon: 'success',
                    title: '{!! \Session::get('success') !!}'
                })
            });
        </script>
    @endif
    @endsection
