<!DOCTYPE html>
<html lang="en">
<body>
    @include('Layouts.header')
    @include('Layouts.sidebar')
    @yield('content')
    @include('Layouts.footer')
</body>
</html>
