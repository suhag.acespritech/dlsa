@extends('Layouts.app')
@section('content')
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
     <!-- jQuery -->
     <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
     <!-- Bootstrap 4 -->
     <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
     <!-- AdminLTE App -->
     <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
     <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0"> Tools Gadgets </h1>
                                {{-- @if (\Session::has('success'))
                                    <div class="alert alert-primary">
                                        <ul>
                                            <li>{!! \Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                @endif
                                @if (\Session::has('fail'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{!! \Session::get('fail') !!}</li>
                                        </ul>
                                    </div>
                                @endif --}}
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Tools Gadgets</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <!-- small box -->
                                <div class="small-box bg-light">
                                    <form class="form-horizontal" method="POST" action="{{ route('toolsgadgets.store') }}">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="label" class="col-sm-2 col-form-label">label
                                                </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="label" class="form-control" id="label"
                                                        placeholder="Enter label name.." value="{{old('label')}}">
                                                    @if ($errors->has('label'))
                                                        <span class="text-danger">{{ $errors->first('label') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="text" class="col-sm-2 col-form-label">Url
                                                </label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="url" class="form-control" id="url" value="{{old('url')}}"
                                                        placeholder="Enter your URL..">
                                                    @if ($errors->has('url'))
                                                        <span class="text-danger">{{ $errors->first('url') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="label" class="col-sm-2 col-form-label">Link
                                                </label>

                                                <div class="col-sm-10">
                                                    <input type="text" name="link" class="form-control" id="link" value="{{old('link')}}"
                                                        placeholder="Enter your link..">
                                                    @if ($errors->has('link'))
                                                        <span class="text-danger">{{ $errors->first('link') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-outline-secondary">submit</button>
                                    <a type="submit" href="{{ route('toolsgadgets.index') }}"
                                    class="btn btn-outline-danger float-right">Cancel</a>
                                   </div>
                                <!-- /.card-footer -->
                                </form>
                            </div>
                        </div>

                    </div>

            </div><!-- /.container-fluid -->
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        </div>
        @if (\Session::has('fail'))
        <script>
            $(document).ready(function() {
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    icon: 'error',
                    title: '{!! \Session::get('fail') !!}'
                })
            });
        </script>
    @endif
    @if (\Session::has('success'))
        <script>
            $(document).ready(function() {
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 3000
                });
                Toast.fire({
                    icon: 'success',
                    title: '{!! \Session::get('success') !!}'
                })
            });
        </script>
    @endif
    @endsection
