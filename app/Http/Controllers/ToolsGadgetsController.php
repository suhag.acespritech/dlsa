<?php

namespace App\Http\Controllers;
use App\Models\tools_gadgets;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ToolsGadgetsController extends Controller
{
     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $image = tools_gadgets::get();
        return view('tools_gadgets.index',compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('tools_gadgets.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'label'=>'required|unique:tools_gadgets',
            'link' => 'required|url',
            'url' => 'required|url',
        ]);
        $image = new tools_gadgets();
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('toolsgadgets.index')->with('success',"Data inserted succesfully.");

    }
    /**
     * Display the specified resource.
     */
    public function show(images $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $images   = tools_gadgets::find($id);
        return view('tools_gadgets.edit',compact('images'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
        $rules = [
            'label' => [
                'required',
                Rule::unique('tools_gadgets')->ignore($request->id),
            ],
            'link' => 'required|url',
            'url' => 'required|url',
        ];
        $validatedData = $request->validate($rules);
        $image = tools_gadgets::find($id);
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('toolsgadgets.index')->with('success',"Selecetd data updated");;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $images = tools_gadgets::find($id);
        $images->delete();
        return redirect()->route('toolsgadgets.index')->with('success',"Selecetd data Deleted");
    }
}
