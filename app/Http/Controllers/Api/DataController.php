<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\categories;
use App\Models\images;
use App\Models\bg_images;
use App\Models\tools_gadgets;
use App\Models\broucher_wallpapers;
use App\Models\social_links;

class DataController extends Controller
{
    public function fetchAllData()
    {
        $categories = categories::with('images')->get();
        $images = images::all();
        $bgImages = bg_images::all();
        $toolsGadgets = tools_gadgets::all();
        $broucherWallpapers = broucher_wallpapers::all();
        $socialLinks = social_links::all();
        return response()->json([
            'categories' => $categories,
            'images' => $images,
            'bg_images' => $bgImages,
            'tools_gadgets' => $toolsGadgets,
            'broucher_wallpapers' => $broucherWallpapers,
            'social_links' => $socialLinks,
        ]);
    }
}
