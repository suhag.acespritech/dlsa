<?php

namespace App\Http\Controllers;

use App\Models\categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Validator;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = categories::get();
        return view('categories.index', ['datas' => $data]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = categories::pluck('name', 'id');
        return view('categories.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required | unique:categories',
        ]);
        $data = new categories();
        $data->name = $request->name;
        $data->parent_id = empty($request->parent_id) ? 0 : $request->parent_id;
        $data->save();
        return redirect()->route('category.index')->with('success', "Data inserted succesfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $categories = categories::find($id);
        $categoryname   = categories::where('id', '!=', $id)->pluck('name', 'id');
        return view('categories.edit', ['category' => $categories, 'categoryname' => $categoryname]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => [
                'required',
                Rule::unique('categories')->ignore($request->id),
            ],
        ];
        $validatedData = $request->validate($rules);
        $category = categories::find($id);
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->save();
        return redirect()->route('category.index')->with('success', "Selecetd data updated");;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $categories = categories::find($id);
        dd($categories->toarray());
        $categories->delete();
        return redirect()->route('category.index')->with('success', "Selecetd data Deleted");
    }
}
