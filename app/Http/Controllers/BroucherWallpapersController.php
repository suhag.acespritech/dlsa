<?php

namespace App\Http\Controllers;

use App\Models\broucher_wallpapers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BroucherWallpapersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $image = broucher_wallpapers::get();
        return view('broucher_wallpapers.index', compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('broucher_wallpapers.create');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'label' => 'required|unique:broucher_wallpapers',
            'link' => 'required|url',
            'url' => 'required|url',
        ]);
        $image = new broucher_wallpapers();
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('broucherwallpapers.index')->with('success', "Data inserted succesfully.");
    }

    /**
     * Display the specified resource.
     */
    public function show(images $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $images   = broucher_wallpapers::find($id);
        return view('broucher_wallpapers.edit', compact('images'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'label' => [
                'required',
                Rule::unique('broucher_wallpapers')->ignore($request->id),
            ],
            'link' => 'required|url',
            'url' => 'required|url',
        ];
        $validatedData = $request->validate($rules);
        $image = broucher_wallpapers::find($id);
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('broucherwallpapers.index')->with('success', "Selecetd data updated");;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $images = broucher_wallpapers::find($id);
        $images->delete();
        return redirect()->route('broucherwallpapers.index')->with('success', "Selecetd data Deleted");
    }
}
