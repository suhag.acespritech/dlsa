<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function postlogin(Request $request)
    {
       $request->validate([
        'username'=>'required',
        'password'=>'required',
       ]);
       $data = $request->only('username','password');

       if(Auth::attempt($data))
       {
         return redirect()->route('dashboard')
                        ->with('success','You have Successfully loggedin');
       }
       else{
        return redirect()->route('login')->with('fail','Wrong User-name or Password');;
       }

    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    // public function registeration()
    // {
    //     return view('register');

    // }
    public function postRegistration(Request $request)
    {
        // $request->validate([
        //     'email' => 'required|unique:register',
        //     'password' => 'required',
        // ]);
        // $data = new user();
        // $data->fisrtName = $request->fisrtName;
        // $data->lastName = $request->lastName;
        // $data->email = $request->email;
        // $data->password = Hash::make($request->password);
        // $data->save();
        // return redirect()->route('register');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function show(auth $auth)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function edit(auth $auth)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, auth $auth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function destroy(auth $auth)
    {
        //
    }
}
