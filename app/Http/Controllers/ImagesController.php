<?php

namespace App\Http\Controllers;

use App\Models\categories;
use App\Models\images;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $image = images::with('category')->get();

        return view('images.index', compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('images.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'label' => 'required|unique:images',
            'link' => 'required|url',
            'url' => 'required|url',
        ]);
        $image = new images();
        $image->category_id = $request->category_id;
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('image.index')->with('success', "Data inserted succesfully.");
    }

    /**
     * Display the specified resource.
     */
    public function show(images $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $images   = images::find($id);
        $categories = categories::get();
        return view('images.edit', compact('images', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'label' => [
                'required',
                Rule::unique('images')->ignore($request->id),
            ],
            'link' => 'required|url',
            'url' => 'required|url',
        ];
        $validatedData = $request->validate($rules);

        $image = images::find($id);
        $image->category_id = $request->category_id;
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('image.index')->with('success', "Selecetd data updated");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $images = images::find($id);
        $images->delete();
        return redirect()->route('image.index')->with('success', "Selecetd data Deleted");
    }
}
