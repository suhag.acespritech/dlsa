<?php

namespace App\Http\Controllers;

use App\Models\social_links;
use Illuminate\Http\Request;

class SocialLinksController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $image = social_links::get();
        return view('social_links.index', compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('social_links.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = $request->validate([

            'link' => 'required|url',
            'image'=> 'required|mimes:jpeg,jpg,png,gif,svg',
            // 'image' => 'required',
        ]);
        $image = new social_links();
        $image->link = $request->link;
        $image->status = !is_null($request->status) ? '1' : '0';
        if ($imagefiles = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "_" . $imagefiles->getClientOriginalName();
            $imagefiles->move($destinationPath, $profileImage);
            $image['image'] = "$profileImage";
        }
        $image->save();

        return redirect()->route('sociallinks.index')->with('success', "Data inserted succesfully.");
    }

    /**
     * Display the specified resource.
     */
    public function show(images $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $images   = social_links::find($id);
        return view('social_links.edit', compact('images'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'link' => 'required|url',

        ]);
        $image = social_links::find($id);
        $image->link = $request->link;
        if ($request->file('image')) {
            unlink(public_path() . '/images/' . $image->image);
            $imagefiles = $request->file('image');
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "_" . $imagefiles->getClientOriginalName();
            $imagefiles->move($destinationPath, $profileImage);
            $image['image'] = "$profileImage";
        }
        $image->status = !is_null($request->status) ? '1' : '0';

        $image->save();
        return redirect()->route('sociallinks.index')->with('success', "Selecetd data updated");;
    }
    public function statusupdate($id)
    {
        $image = social_links::find($id);
        $image->status = $image->status == 1 ? '0':'1';
        $image->save();
        return response()->json(['status'=>'success','message'=>'status data updated'],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $images = social_links::find($id);
        if (!empty($images->image)) {
            unlink(public_path() . '/images/' . $images->image);
        }

        $images->delete();
        return redirect()->route('sociallinks.index')->with('success', "Selecetd data Deleted");
    }
}
