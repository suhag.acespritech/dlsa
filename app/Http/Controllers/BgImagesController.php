<?php
namespace App\Http\Controllers;
use App\Models\bg_images;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Rules\ValidUrl;

class BgImagesController extends Controller
{
      /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $image = bg_images::get();
        return view('bg_images.index',compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('bg_images.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

// Perform validation
try {
    $validated = $request->validate([
        'label' => 'required|unique:bg_images',
        'link' => 'required|url',
        'url' => 'required|url',
    ]);
    // Validation passed, proceed with your logic
    // e.g., saving data to the database
} catch (\Illuminate\Validation\ValidationException $e) {
    // Validation failed, return errors
    return redirect()->back()->withErrors($e->errors())->withInput();
}
        $image = new bg_images();
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('bgimage.index')->with('success',"Data inserted succesfully.");
    }

    /**
     * Display the specified resource.
     */
    public function show(images $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $images   = bg_images::find($id);
        return view('bg_images.edit',compact('images'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$id)
    {
        $rules = [
            'label' => [
                'required',
                Rule::unique('bg_images')->ignore($request->id),
            ],
            'link'=>'required|url',
            'url'=>'required|url',
        ];
        $validatedData = $request->validate($rules);
        $image = bg_images::find($id);
        $image->label = $request->label;
        $image->link = $request->link;
        $image->url = $request->url;
        $image->save();
        return redirect()->route('bgimage.index')->with('success',"Selecetd data updated");;

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $images = bg_images::find($id);
        $images->delete();
        return redirect()->route('bgimage.index')->with('success',"Selecetd data Deleted");
    }
}
