<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class broucher_wallpapers extends Model
{
    use HasFactory;
    protected $table = 'broucher_wallpapers';
}
