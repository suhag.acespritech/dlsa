<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tools_gadgets extends Model
{
    use HasFactory;
    protected $table='tools_gadgets';

}
