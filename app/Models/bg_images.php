<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bg_images extends Model
{
    use HasFactory;
    protected $table = 'bg_images';
}
