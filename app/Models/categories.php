<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    public $table='categories';
    public $fillable=['name','parent_id'];

    public static function getCategory()
    {
         return categories::pluck('name','id');

    }
    public function images()
    {
        return $this->belongsTo(images::class, 'id', 'category_id');
    }

}
