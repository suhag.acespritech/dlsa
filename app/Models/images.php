<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class images extends Model
{
    use HasFactory;
    public $table='images';
    protected $fillable=['category_id','label','url','link'];

    public function category()
    {
       return $this->belongsTo(categories::class);
    }

}
